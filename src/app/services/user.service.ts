import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User} from '../models/user.model';
import { Login} from '../models/login.model';
import { JwtResponse} from '../models/jwt-response.model';

@Injectable({
  providedIn: 'root'
})
export class UserService { 
  private loginUrl = 'https://localhost:5001/api/users/auth';
  private signupUrl = 'https://localhost:5001/api/users/register';
  private baseUrl = 'https://localhost:5001/api/users';

  constructor(private http: HttpClient) { }

  attemptAuth(credentials: Login): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials);
  }

  signUp(info: User): Observable<string> {
    return this.http.post<string>(this.signupUrl, info);
  }

  getUserList(): Observable<User[]> {
    return this.http.get<User []>(`${this.baseUrl}`);
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  getUserByName(username:string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/name/${username}`);
  }

  getUserSt(username: string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/name/${username}`);
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}`, user);
  }
 
  updateUser(id: number, user: User): Observable<User> {
    return this.http.put<User>(`${this.baseUrl}/${id}`, user);
  }
 
  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

}

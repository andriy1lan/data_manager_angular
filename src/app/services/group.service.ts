import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group} from '../models/group.model';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private baseUrl = 'https://localhost:5001/api/groups';

  constructor(private http: HttpClient) { }

  getGroupList(): Observable<Group[]> {
    return this.http.get<Group []>(`${this.baseUrl}`);
  }

  getGroup(id: number): Observable<Group> {
    return this.http.get<Group>(`${this.baseUrl}/${id}`);
  }

  createGroup(group: Group): Observable<Group> {
    return this.http.post<Group>(`${this.baseUrl}`, group);
  }
 
  updateGroup(id: number, group: Group): Observable<Group> {
    return this.http.put<Group>(`${this.baseUrl}/${id}`, group);
  }
 
  deleteGroup(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}

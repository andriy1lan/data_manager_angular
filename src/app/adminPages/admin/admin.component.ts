import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { UserService } from '../../services/user.service';
import { User} from '../../models/user.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  editingForm: FormGroup;
  isAdmin:boolean=false;
  isediting:boolean=false;
  userid:number=0;
  user: User;
  num:number;
  users1: User[];
  users: Observable<User[]>;

  constructor(private userService: UserService) { 
    this.editingForm= new FormGroup({
      name: new FormControl(''),
      username: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      isAdmin: new FormControl(false)
    })
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.userService.getUserList().subscribe(data=>{this.users1=data;
      this.users1.sort((x,y) => x.id < y.id ? -1 : 1);
      this.num=this.users1.length});
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id) //this.user.id
      .subscribe(
        data => {
          console.log(data);
          //this.reloadData1();
          let i:number;
          i=this.users1.findIndex(x => x.id === id);
          if (i!=-1) {this.users1.splice(i, 1); this.num--;}
        },
        error => console.log(error));
  }

  updateUser1(id: number) {
    //preventing editing two rows simultaneously
    if (this.isediting && this.userid!=0 && this.userid!=id) return;

    this.user = this.users1.filter(u=>u.id===id)[0];
    if (!this.isediting) {this.userid=id;}
    else {this.userid=0;}
    console.log(this.user);
    if (!this.isediting) {
    this.editingForm.setValue({
      name: this.user.name,
      username: this.user.userName,
      email: this.user.email,
      password:'',
      isAdmin: this.user.isAdmin
    });
      }
    console.log(this.editingForm);
    console.log(this.editingForm.value.isAdmin);

    if (this.isediting) {
    this.user.name=this.editingForm.value.name;
    this.user.userName=this.editingForm.value.username;
    this.user.email=this.editingForm.value.email;
    this.user.password='';
    this.user.isAdmin=this.editingForm.value.isAdmin;
    this.userService.updateUser(this.user.id, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    console.log(JSON.stringify(this.user));
    console.log(this.isediting);
    console.log(this.userid);
    this.editingForm.reset();
    }
    this.isediting=!this.isediting;
  }

}

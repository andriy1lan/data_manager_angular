import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { GroupService } from '../../services/group.service';
import { UserService } from '../../services/user.service';
import { Group } from '../../models/group.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  iscreating:boolean=false;
  isediting:boolean=false;
  groupId:number=0;
  newGroup: Group;
  editedGroup: Group;
  num:number;
  groups: Group[];
  users:User[];
  allUsers:User[];
  otherUsers:User[];
  groups1: Observable<Group[]>;
  isCreated:boolean = false;
  message:string="";

  constructor(private groupService: GroupService, private userService :UserService) { }

  ngOnInit() {
    this.reloadData();
    this.userService.getUserList().subscribe(data=>{this.allUsers=data;
      console.log(this.allUsers); //get all users
      this.allUsers.sort((x,y) => x.id < y.id ? -1 : 1)});
  }

  reloadData() {
    this.groupService.getGroupList().subscribe(data=>{this.groups=data;
      console.log(this.groups);
      this.groups.sort((x,y) => x.groupID < y.groupID ? -1 : 1);
      this.num=this.groups.length});
  }

  showAddNewGroup() {
    this.iscreating=true;
    this.users=this.allUsers.map(obj=>({...obj}));
    this.newGroup=new Group();
  }

  createGroup() {
    this.newGroup.users=this.users.filter(u=>u.checked==true);
    this.newGroup.usersToRemove=false;
    console.log(this.newGroup);
    this.groupService.createGroup(this.newGroup).subscribe(
      data => {
        console.log(data);
        this.isCreated = true;
        this.message="New group created";
        this.reloadData();
      },
      error => {
        console.log(error);
        this.message="Error in group creation";
        //this.errorMessage = error.error.message;
      }
    );
    this.newGroup=null;
    this.iscreating=false;
  }

  editGroup(id: number) {
    this.isediting=true;
    this.editedGroup = this.groups.filter(g=>g.groupID===id)[0];
    this.otherUsers=this.allUsers.filter(u=>this.editedGroup.users.filter(e=>e.id===u.id).length===0);
  }

  updateGroup(id: number) {
    this.otherUsers=this.otherUsers.filter(o=>o.checked==true);
    this.isediting=false;
    console.log(this.otherUsers);
    console.log(this.allUsers);
    let usersToDelete:User[]=this.editedGroup.users.filter(u=>u.checked==true);
    console.log(usersToDelete);
    this.editedGroup.users=usersToDelete;
    this.editedGroup.usersToRemove=true;
    console.log(this.editedGroup);
    this.groupService.updateGroup(id,this.editedGroup).subscribe(
      data => {
        console.log(data);
        this.message="Group updated";
        this.reloadData();
      },
      error => {
        console.log(error);
        this.message="Error in group updating";
      }
    );
    this.editedGroup.users=this.otherUsers;
    this.editedGroup.usersToRemove=false;
    console.log(this.editedGroup);
    this.groupService.updateGroup(id,this.editedGroup).subscribe(
      data => {
        console.log(data);
        this.message="Group updated";
        this.reloadData();
      },
      error => {
        console.log(error);
        this.message="Error in group updating";
      }
    );
    this.editedGroup=null;
    this.otherUsers=null;
  }

  deleteGroup(id: number) {
    this.groupService.deleteGroup(id) //this.user.id
      .subscribe(
        data => {
          console.log(data);
          let i:number;
          i=this.groups.findIndex(x => x.groupID === id);
          if (i!=-1) {this.groups.splice(i, 1); this.num--;}
          this.message="Group deleted";
          this.reloadData();
        },
        error => console.log(error));
  }



}

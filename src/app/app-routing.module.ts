import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './authPages/home/home.component';
import { LoginComponent } from './authPages/login/login.component';
import { RegisterComponent } from './authPages/register/register.component';
import { AdminComponent } from './adminPages/admin/admin.component';
import { GroupComponent } from './adminPages/group/group.component';
import { FileComponent } from './filePage/file/file.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'adminpane', component: AdminComponent}, //canActivate: [Adminguard] 
  { path: 'groups', component: GroupComponent},
  { path: 'files', component: FileComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../services/token-storage.service';
import { UserService } from '../../services/user.service';
import { Login } from '../../models/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = 'Check username/password';
  role: string;
  username:string;
  private loginInfo: Login;

  constructor(private userService: UserService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.role = this.tokenStorage.getAuthority();
      this.username = this.tokenStorage.getUsername();
    }
  }
  
  onSubmit() {   //onSubmit(myForm: NgForm)
    console.log(this.form);
 
      this.loginInfo = new Login(
      this.form.username,
      this.form.password);
 
    this.userService.attemptAuth(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthority(data.authority);
 
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.username=this.tokenStorage.getUsername();
        this.role = this.tokenStorage.getAuthority();
        this.reloadPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.text;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage() {
    window.location.href='/home';
  }

}

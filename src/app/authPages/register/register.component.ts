import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  signupInfo: User;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';
  isAdmin: boolean = false;

  constructor(private userService: UserService) { }
  ngOnInit() {
  }

  onSubmit() {
    console.log(this.form);
 
    this.signupInfo = new User(
      this.form.name,
      this.form.username,
      this.form.email,
      this.form.password,
      this.form.isAdmin?true:false);
      console.log(this.signupInfo);
      this.userService.signUp(this.signupInfo).subscribe(
        data => {
          console.log(data);
          this.isSignedUp = true;
          this.isSignUpFailed = false;
        },
        error => {
          console.log(error);
          this.errorMessage = error.error.message;
          this.isSignUpFailed = true;
        }
      );
  }

}

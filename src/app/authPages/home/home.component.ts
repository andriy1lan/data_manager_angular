import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../services/token-storage.service';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  info: any;
  isEditing: boolean;
  editedUser: User;
  message:string;

  constructor(private token: TokenStorageService, private userService: UserService) { }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthority()
    };
  }

  editUser() {
    this.isEditing=true;
    this.userService.getUserByName(this.info.username).subscribe(
      data => {
        this.editedUser=data;
        console.log(data);
      },
      error => {
        console.log(error);
        this.message = "Failed to get user data";
      }
    );
    console.log(this.editedUser);
  }

  updateUser() {
    console.log(this.editedUser);
      this.userService.updateUser(this.editedUser.id,this.editedUser).subscribe(
        data => {
          console.log(data);
          this.message = "Your user data updated";
          this.isEditing=false;
        },
        error => {
          console.log(error);
          this.message = "User data update failed";
        }
      );
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }

}

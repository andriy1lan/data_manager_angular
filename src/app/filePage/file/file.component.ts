import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse} from '@angular/common/http';


@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  public message: string;
  public response: {dbPath: ''};
  public files: string []=[];
  public fileUrl: string;
  //httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })};

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getFiles();
  }

  
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    } 
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    console.log(formData.get('file'));
      this.http.post('https://localhost:5001/api/files/upload', formData)
      .subscribe(response => {
        console.log(response);
        this.message="File "+(<any>response).dbPath.substring(13)+" uploaded";
        this.getFiles();
      },
      error => {
        console.log(error);
        this.message="Error in file upload";
      }); 
  }

  private getFiles = () => {
    this.http.get(`https://localhost:5001/api/files/getFiles`).subscribe(data => this.files = data['files']);
  }

  public download = (file)=> {
    console.log(file);
    this.fileUrl=file; let params = new HttpParams().set('fileUrl', file);
    this.http.get('https://localhost:5001/api/files/download',{
      responseType: 'blob',
      params:params
  }).subscribe((response) => {
      this.message ="file is downloading";
      console.log(response);
      this.downloadFile(response);
      this.message ="file was downloaded";
  });
  }

  private downloadFile(data: Blob) { //HttpResponse<Blob>
    //const downloadedFile1 = new Blob([data.body], { type: data.body.type });
    const downloadedFile=data;
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = this.fileUrl;
    a.href = URL.createObjectURL(downloadedFile);
    a.target = '_blank';
    a.click();
    document.body.removeChild(a);
}

}

import {User} from './user.model';
export class Group {
    groupID: number;
    name: string;
    users: User[];
    usersToRemove:boolean;
    constructor(name?: string, users?: User[],
        userstoremove?:boolean) {
            this.name=name;
            this.users=users;
            this.usersToRemove=userstoremove;
        }
}

export class User {
    id: number;
    name: string;
    userName: string;
    email: string;
    password: string;
    isAdmin:boolean;
    checked:boolean=false;
    constructor(name: string, username: string, email: string, 
        password: string, isadmin:boolean) {
        this.name = name;
        this.userName = username;
        this.email = email;
        this.password = password;
        this.isAdmin=isadmin;
        }
}

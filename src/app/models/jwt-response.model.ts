export class JwtResponse {
    token: string;
    username: string;
    authority: string;
}
